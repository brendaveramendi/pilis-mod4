import { Router } from 'express';
import {
    getBooking,
    createBooking,   
    updateBooking,
    deleteBooking,
    getBookings

} from '../controller/booking.controller';
import passport from 'passport';

const router = Router();

router.get('/booking', passport.authenticate('jwt', { session: false }), getBookings);
router.get('/booking/:id', passport.authenticate('jwt', { session: false }), getBooking);
router.post('/booking/user/:userId/event/:eventId', passport.authenticate('jwt', { session: false }), createBooking);
router.put('/booking/:id', passport.authenticate('jwt', { session: false }), updateBooking);
router.delete('/booking/:id', passport.authenticate('jwt', { session: false }), deleteBooking);

export default router;