import { Router } from 'express';
import {
    getUser,
    createUser,
    updateUser,
    deleteUser,
    signUp,
    signIn,
    refresh,
    protectedEndpoint,
    getUsers
} from '../controller/user.controller';
import passport from 'passport';

const router = Router();

router.get('/users/', passport.authenticate('jwt', { session: false }), getUsers);
router.get('/users/:id', passport.authenticate('jwt', { session: false }), getUser);
//router.post('/users', createUser);
router.put('/users/:id', passport.authenticate('jwt', { session: false }), updateUser);
router.delete('/users/:id', passport.authenticate('jwt', { session: false }), deleteUser);

//Agregar para jwt
router.post('/signup', passport.authenticate('jwt', { session: false }), signUp); //Create Users
router.post('/signin', signIn); //Login
//router.post('/token', refresh);
//router.post('/protected', passport.authenticate('jwt', { session: false }), protectedEndpoint);

export default router;