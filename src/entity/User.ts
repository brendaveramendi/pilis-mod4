import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  BaseEntity,
  OneToMany
} from "typeorm";
import { Booking } from "./Booking";
@Entity() // se puede pasar como parametro el nombre de tabla ej: 'usersTable'

export class User extends BaseEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  username: string;

  @Column()
  password: string;

  //-------Relationships-----------
  @OneToMany(
  ()=>Booking,
  bookings => bookings.user
  )
  
  bookings: Booking[]
  //--------------------------------
}
