import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  BaseEntity,
  OneToMany,
  CreateDateColumn
} from "typeorm";
//import { Geometry } from "typeorm";
import { Booking } from "./Booking";
import { Geometry } from "typeorm";

export enum EventsTypes {
  CINEMA = 'cinema',
  THEATER = 'theater',
  RECITAL = 'recital',
  CHAT = 'chat'
  }

@Entity()
export class Events extends BaseEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @Column()
  description: string;

  @Column()
  place: string;

  @CreateDateColumn()
  date: Date;

  @Column()
  gps: string;

  @Column({
    type:"numeric"
  })
  price:number;

  @Column({default:0})
  limit: number;

  @Column({
    type:"enum",
    enum:EventsTypes
  })
  eventType:string; 
  //-------Relationships-----------

  @OneToMany(
    ()=>Booking,
    bookings => bookings.event
  )
    
  bookings: Booking[]

  //-------------------------------

 
}
