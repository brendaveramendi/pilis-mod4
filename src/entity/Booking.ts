import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  BaseEntity,
  ManyToOne,
  JoinColumn,
  CreateDateColumn
} from "typeorm";
import { Events } from "./Events";
import { User } from "./User";
import { Geometry } from "typeorm";


@Entity()

export class Booking extends BaseEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({
    type:"numeric"
  })
  price:number;

  @CreateDateColumn()
  date: Date;

  @Column()
  place: string;

  @Column()
  gps:string;

  //-------Relationships-----------
  @ManyToOne(
      () => Events,
      event => event.bookings
  )

  @JoinColumn({
    name: 'id_events',
  })
  event: Events

  @ManyToOne(
  () => User,
    user => user.bookings,
  )

  @JoinColumn({
    name: 'id_user',
  })
  user: User

  //-------------------------------

}
