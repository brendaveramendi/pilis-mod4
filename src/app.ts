import express from 'express';
import morgan from 'morgan';
import cors from 'cors';
import userRoutes from  './routes/user.routes';
import eventRoutes from  './routes/event.routes';
import bookingRoutes from  './routes/booking.routes';

import passportMiddleware from './middlewares/passport';
import passport from 'passport';
import passportLocal from 'passport-local';

const app = express()

app.use(morgan('dev'));
app.use(cors());
app.use(express.json());

//Agregar para jwt
app.use(express.urlencoded({extended: false}));
app.use(passport.initialize());
passport.use(passportMiddleware);

// Routes
app.use('/api', userRoutes);
app.use('/api', eventRoutes);
app.use('/api', bookingRoutes);
export default app;