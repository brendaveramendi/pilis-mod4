import "reflect-metadata";
import app from './app';
import { AppDataSource } from './db'
const port = 3000;

//Initialize : Lee los datos de db y se inicializa  la base de datos
async function main() {
    try{
        await AppDataSource.initialize();
        console.log('Database conected.....');
        app.listen(port, ()=> console.log(`App listening on port ${port}`))
    }catch (error){
        console.log(error)
    }
}

main();