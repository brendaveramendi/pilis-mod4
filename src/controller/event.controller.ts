import { Request, Response } from 'express';
import { Events, EventsTypes} from '../entity/Events';

export const getEvents = async (req: Request, res: Response) => {
    //console.log('Entrando....');
    //.find() es una funcion osrm que trae todos los ususarios
    try{
        const events = await Events.find();
        //console.log('Events -->'), events;
        return res.json(events);
    }catch(error){
        if(error instanceof Error){
            return res.status(500).json({message: error.message});
        }
    }
  }

export const getEvent = async (req: Request, res: Response) => {
    try{
        const { id } = req.params;
        const event = await Events.findOneBy({id: parseInt(id)});

        if(!event) return res.status(404).json({ message: 'Event not found'})
        return res.json(event);
    }catch(error){
        if(error instanceof Error){
            return res.status(500).json({message: error.message})
        }
    }
}

export const createEvent = async (req: Request, res: Response) => {
    const {name, description, place,date,gps, price,limit,eventType} = req.body;
    //Creamos el user que vamos a persistir
    const event = new Events();
    event.name = name;
    event.description = description;
    event.place = place;
    event.date = date;
    event.gps = gps;
    event.price = price;
    event.limit = limit;
    if(eventType == EventsTypes.CHAT || eventType == EventsTypes.CINEMA || eventType == EventsTypes.RECITAL || eventType == EventsTypes.THEATER ){
        event.eventType = eventType
    }else{
        return res.json({message: "Event type not found"})
    }
    //Se guarda en la base de datos 
    await event.save();
    return res.json(event);
}

export const updateEvent = async (req: Request, res: Response) => {
    const { id } = req.params;
    try{
        const event = await Events.findOneBy({ id: parseInt(id)});
        if(!event) return res.status(404).json({ message: 'Not Event found'});
        await Events.update({ id: parseInt(id)}, req.body);
        return res.sendStatus(204);
    }
    catch(error){
        if(error instanceof Error){
            return res.status(500).json({ message: error.message });
        }
    }
}

export const deleteEvent = async (req: Request, res: Response) => {
    const { id } = req.params;
    try {
      const result = await Events.delete({ id: parseInt(id) });
  
      if (result.affected === 0)
        return res.status(404).json({ message: "Event not found" });
  
      return res.sendStatus(204);
    } catch (error) {
      if (error instanceof Error) {
        return res.status(500).json({ message: error.message });
      }
    }
}
