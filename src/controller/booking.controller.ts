import { Request, Response } from 'express';
import { Booking } from '../entity/Booking';
import { User } from '../entity/User';
import { Events } from '../entity/Events';

export const getBookings = async (req: Request, res: Response) => {
    try{
        const bookings = await Booking.find();
        //console.log('Events -->'), events;
        return res.json(bookings);
    }catch(error){
        if(error instanceof Error){
            return res.status(500).json({message: error.message});
        }
    }
}

export const getBooking = async (req: Request, res: Response) => {
    try{
        const { id } = req.params;
        const booking = await Booking.findOneBy({id: parseInt(id)});
        if(!booking) return res.status(404).json({ message: 'Booking not found'})
        return res.json(booking);
    }catch(error){
        if(error instanceof Error){
            return res.status(500).json({message: error.message})
        }
    }
  }

  export const createBooking = async (req: Request, res: Response) => {
    const {userId, eventId } = req.params;
    
   
    if(!userId){
         return res.json({message: "User not found"})
    }
    
    if(!eventId){
         return res.json({message: "Event not found"})
    }    
    const booking = new Booking();
    booking.event = new Events();
    booking.user = new User();
    
    const elementUser = await User.findOne( { where:
        { id: parseInt(userId) }
    });
    const elementEvent = await Events.findOne( { where:
        { id: parseInt(eventId) }
    });

  
    if(elementEvent!.limit == 0){
       booking.price = elementEvent!.price;
       booking.place = elementEvent!.place;
       booking.date = elementEvent!.date;
       booking.gps = elementEvent!.gps;
       booking.event = elementEvent!;
       booking.user = elementUser!;   
       await booking.save();
       return res.json(booking);
    }else if ( elementEvent!.limit >0 ) {
        booking.price = elementEvent!.price;
        booking.place = elementEvent!.place;
        booking.date = elementEvent!.date;
        booking.gps = elementEvent!.gps;
        booking.event = elementEvent!;
        booking.user = elementUser!;
        elementEvent!.limit -= 1;   
        if(elementEvent!.limit == 0){
            elementEvent!.limit -= 1;
        } 
        await booking.save();
        await elementEvent.save()
        return res.json(booking);
    }else{
        return res.json({message: "Sold out"})
    }
}

export const updateBooking = async(req: Request, res:Response) =>{
    const { id } = req.params; 
    try{
        const booking = await Booking.findOneBy({ id: parseInt(id)});
        if(!booking) return res.status(404).json({ message: 'Not booking found '});
        await Booking.update({id: parseInt(id)}, req.body);
        return res.sendStatus(204);
    }catch(error){
        if (error instanceof Error){
            return res.status(500).json({ message: error.message});
        }
    }
}

export const deleteBooking = async (req: Request, res: Response) => {
    const { id } = req.params;
    try {
      const result = await Booking.delete({ id: parseInt(id) });
  
      if (result.affected === 0)
        return res.status(404).json({ message: "Booking not found" });
  
      return res.sendStatus(204);
    } catch (error) {
      if (error instanceof Error) {
        return res.status(500).json({ message: error.message });
      }
    }
  };