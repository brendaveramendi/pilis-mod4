import { DataSource } from 'typeorm';
import { User } from './entity/User';
import { Events } from './entity/Events';
import { Booking } from './entity/Booking';

//new DataSource configura la conexion a la base de datos
export const AppDataSource = new DataSource({
    type: "mysql",
    host:"localhost",
    port: 3306,
    username: "root",
    password: "mysql",
    database: "ticket-system",
    synchronize: true,
    entities: [User,Events,Booking],
})