## Sistema de reserva de tickets 🔙

### Dependencias empleadas

<ul>
<li>Bcrypt 5.1.0</li>
<li>Cors 2.8.5</li>
<li>Express 4.18.2</li>
<li>Jsonwebtoken 9.0.1</li>
<li>Morgan 1.10.0</li>
<li>Mysql 2.18.1</li>
<li>Passport 0.6.0</li>
<li>Passport-jwt 4.0.1</li>
<li>Passport-local 1.0.0</li>
<li>Reflect-metadata 0.1.13</li>
<li>Typeorm</li>
</ul>

## Funcionalidad

<ul>
<li>El sistema contempla las API Rest de users, events y tickets de reserva.</li>
<li>Se valida si el límite de cupos de la entidad evento es menor o igual al límite de cupos, antes de registrar un booking</li>
<li>Si el el límite es cero, entonces el evento no tiene límite de cupo</li>
<li>Se establece una relación entre la entidad events y la entidad booking de 1:n y una relación entre users y booking de 1:n</li>
</ul>

  
## Ejecución 

 Clonar el proyecto: https://gitlab.com/brendaveramendi/pilis-mod4.git
#### Ejecutar
* docker-compose up -d 
* npm install 
* npm run dev

## Programadores

Ordoñez Gustavo Alberto 👨‍💻

Veramendi Brenda Noelia 👩‍💻
