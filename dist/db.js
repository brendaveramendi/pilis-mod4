"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.AppDataSource = void 0;
const typeorm_1 = require("typeorm");
const User_1 = require("./entity/User");
const Events_1 = require("./entity/Events");
const Booking_1 = require("./entity/Booking");
//new DataSource configura la conexion a la base de datos
exports.AppDataSource = new typeorm_1.DataSource({
    type: "mysql",
    host: "localhost",
    port: 3306,
    username: "root",
    password: "mysql",
    database: "ticket-system",
    synchronize: true,
    entities: [User_1.User, Events_1.Events, Booking_1.Booking],
});
