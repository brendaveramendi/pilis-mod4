"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.deleteEvent = exports.updateEvent = exports.createEvent = exports.getEvent = exports.getEvents = void 0;
const Events_1 = require("../entity/Events");
const getEvents = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    console.log('Entrando....');
    //.find() es una funcion osrm que trae todos los ususarios
    try {
        const events = yield Events_1.Events.find();
        console.log('Events -->'), events;
        return res.json(events);
    }
    catch (error) {
        if (error instanceof Error) {
            return res.status(500).json({ message: error.message });
        }
    }
});
exports.getEvents = getEvents;
const getEvent = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const { eventId } = req.params;
        const event = yield Events_1.Events.findOneBy({ id: parseInt(eventId) });
        if (!event)
            return res.status(404).json({ message: 'Event not found' });
        return res.json(event);
    }
    catch (error) {
        if (error instanceof Error) {
            return res.status(500).json({ message: error.message });
        }
    }
});
exports.getEvent = getEvent;
const createEvent = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const { name, description, place, date, gps, price, limit, eventType } = req.body;
    //Creamos el user que vamos a persistir
    const event = new Events_1.Events();
    event.name = name;
    event.description = description;
    event.place = place;
    event.date = date;
    event.gps = gps;
    event.price = price;
    event.limit = limit;
    if (eventType == Events_1.EventsTypes.CHAT || eventType == Events_1.EventsTypes.CINEMA || eventType == Events_1.EventsTypes.RECITAL || eventType == Events_1.EventsTypes.THEATER) {
        event.eventType = eventType;
    }
    else {
        return res.json({ message: "Event type not found" });
    }
    //Se guarda en la base de datos 
    yield event.save();
    return res.json(event);
});
exports.createEvent = createEvent;
const updateEvent = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const { id } = req.params;
    try {
        const event = yield Events_1.Events.findOneBy({ id: parseInt(id) });
        if (!event)
            return res.status(404).json({ message: 'Not Event found' });
        yield Events_1.Events.update({ id: parseInt(id) }, req.body);
        return res.sendStatus(204);
    }
    catch (error) {
        if (error instanceof Error) {
            return res.status(500).json({ message: error.message });
        }
    }
});
exports.updateEvent = updateEvent;
const deleteEvent = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const { id } = req.params;
    try {
        const result = yield Events_1.Events.delete({ id: parseInt(id) });
        if (result.affected === 0)
            return res.status(404).json({ message: "Event not found" });
        return res.sendStatus(204);
    }
    catch (error) {
        if (error instanceof Error) {
            return res.status(500).json({ message: error.message });
        }
    }
});
exports.deleteEvent = deleteEvent;
