"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.deleteBooking = exports.updateBooking = exports.createBooking = exports.getBooking = void 0;
const Booking_1 = require("../entity/Booking");
const User_1 = require("../entity/User");
const Events_1 = require("../entity/Events");
const getBooking = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const { id } = req.params;
        const booking = yield Booking_1.Booking.findOneBy({ id: parseInt(id) });
        if (!booking)
            return res.status(404).json({ message: 'Event not found' });
        return res.json(booking);
    }
    catch (error) {
        if (error instanceof Error) {
            return res.status(500).json({ message: error.message });
        }
    }
});
exports.getBooking = getBooking;
const createBooking = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const { userId, eventId } = req.params;
    // const{ price, place} = req.body
    //Creamos el user que vamos a persistir
    if (!userId) {
        return res.json({ message: "User not found" });
    }
    if (!eventId) {
        return res.json({ message: "Event not found" });
    }
    const booking = new Booking_1.Booking();
    booking.event = new Events_1.Events();
    booking.user = new User_1.User();
    const event = new Events_1.Events();
    const elementUser = yield User_1.User.findOne({ where: { id: parseInt(userId) }
    });
    const elementEvent = yield Events_1.Events.findOne({ where: { id: parseInt(eventId) }
    });
    booking.price = elementEvent.price;
    booking.place = elementEvent.place;
    booking.date = elementEvent.date;
    booking.gps = elementEvent.gps;
    booking.event = elementEvent;
    booking.user = elementUser;
    yield booking.save();
    return res.json(booking);
    // if(elementEvent!.limit - 1 >= 0){
    //     event.limit = elementEvent!.limit-1
    //     await event.save();
    //     booking.price = price;
    //     booking.place = place;
    //     booking.event = elementEvent!;
    //     booking.user = elementUser!;    
    // await booking.save();
    // return res.json(booking);
    // }else{
    //     return res.json({message: ""})
    // }
});
exports.createBooking = createBooking;
const updateBooking = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const { id } = req.params;
    try {
        const booking = yield Booking_1.Booking.findOneBy({ id: parseInt(id) });
        if (!booking)
            return res.status(404).json({ message: 'Not booking found ' });
        yield Booking_1.Booking.update({ id: parseInt(id) }, req.body);
        return res.sendStatus(204);
    }
    catch (error) {
        if (error instanceof Error) {
            return res.status(500).json({ message: error.message });
        }
    }
});
exports.updateBooking = updateBooking;
const deleteBooking = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const { id } = req.params;
    try {
        const result = yield Booking_1.Booking.delete({ id: parseInt(id) });
        if (result.affected === 0)
            return res.status(404).json({ message: "Booking not found" });
        return res.sendStatus(204);
    }
    catch (error) {
        if (error instanceof Error) {
            return res.status(500).json({ message: error.message });
        }
    }
});
exports.deleteBooking = deleteBooking;
