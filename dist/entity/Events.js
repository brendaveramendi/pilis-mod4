"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Events = exports.EventsTypes = void 0;
const typeorm_1 = require("typeorm");
//import { Geometry } from "typeorm";
const Booking_1 = require("./Booking");
var EventsTypes;
(function (EventsTypes) {
    EventsTypes["CINEMA"] = "cinema";
    EventsTypes["THEATER"] = "theater";
    EventsTypes["RECITAL"] = "recital";
    EventsTypes["CHAT"] = "chat";
})(EventsTypes || (exports.EventsTypes = EventsTypes = {}));
let Events = exports.Events = class Events extends typeorm_1.BaseEntity {
};
__decorate([
    (0, typeorm_1.PrimaryGeneratedColumn)(),
    __metadata("design:type", Number)
], Events.prototype, "id", void 0);
__decorate([
    (0, typeorm_1.Column)(),
    __metadata("design:type", String)
], Events.prototype, "name", void 0);
__decorate([
    (0, typeorm_1.Column)(),
    __metadata("design:type", String)
], Events.prototype, "description", void 0);
__decorate([
    (0, typeorm_1.Column)(),
    __metadata("design:type", String)
], Events.prototype, "place", void 0);
__decorate([
    (0, typeorm_1.Column)({ default: "0000-00-00 00:00" }),
    __metadata("design:type", String)
], Events.prototype, "date", void 0);
__decorate([
    (0, typeorm_1.Column)({ default: "00,0000 00,0000" }),
    __metadata("design:type", String)
], Events.prototype, "gps", void 0);
__decorate([
    (0, typeorm_1.Column)({
        type: "numeric"
    }),
    __metadata("design:type", Number)
], Events.prototype, "price", void 0);
__decorate([
    (0, typeorm_1.Column)({ default: 0 }),
    __metadata("design:type", Number)
], Events.prototype, "limit", void 0);
__decorate([
    (0, typeorm_1.Column)({
        type: "enum",
        enum: EventsTypes
    }),
    __metadata("design:type", String)
], Events.prototype, "eventType", void 0);
__decorate([
    (0, typeorm_1.OneToMany)(() => Booking_1.Booking, bookings => bookings.event),
    __metadata("design:type", Array)
], Events.prototype, "bookings", void 0);
exports.Events = Events = __decorate([
    (0, typeorm_1.Entity)()
], Events);
